<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <link rel="stylesheet" href="CSS/theme.css" type="text/css">
</head>
<title>Register</title>
<body>
  <?php
  // validate date format dd/MM/yyyy
  function isDate($string)
  {
    $regex = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
    if (!preg_match($regex, $string)) return false;
    return true;
  }
  // Code PHP xử lý validate
  $error = array();
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    // Kiểm tra định dạng dữ liệu
    if (empty($_POST['fullName'])) {
      $error['fullName'] = 'Hãy nhập tên.';
    }

    if (empty($_POST['gender'])) {
      $error['gender'] = 'Hãy chọn giới tính.';
    }
    if (empty($_POST['khoa'])) {
      $error['khoa'] = 'Hãy chọn phân khoa.';
    }
    if (empty($_POST['birthday'])) {
      $error['birthday'] = 'Hãy nhập ngày sinh.';
    }elseif(!isDate($_POST['birthday'])) {
      $error['birthdayFormat'] = 'Hãy nhập ngày sinh đúng định dạng.';
    }
    $file =  $_FILES["filename"]["name"];
    $files = $_FILES["filename"]["tmp_name"];
    $filename = pathinfo($file, PATHINFO_FILENAME);
    $fileExtension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
    $allowedExtension = array('jpg', 'jpeg', 'png', 'gif');
    if (in_array($fileExtension, $allowedExtension)) {
      date_default_timezone_set('Asia/Ho_Chi_Minh');
      $date_time = date('YmdHis', time());
      $_SESSION['image'] = $filename . "_" . $date_time . "." . $fileExtension;
      if (!file_exists('upload')) {
        mkdir('upload', 0777, true);
      }
      $path = "upload/" . $filename . "_" . $date_time . "." . $fileExtension;
      move_uploaded_file($files, $path);
          
    }else{
      $error["filename"] = "Hãy chọn ảnh có định dạng jpg, jpeg, png hoặc gif.";
    }


    if(empty($error)){
      $_SESSION["Regist"] = $_POST;
      header("Location: confirm.php");
    }
  }
  ?>
  <div class="container">
      <form action="index.php" method="POST" id="form" enctype="multipart/form-data">
        <?php
        if ($error) {
          foreach ($error as $key => $value) {
            echo '
                <p style="color: red">' . $value . '</p>
              ';
          }
        }
        ?>
        <div class="user_input">
          <lable>Họ và tên
            <span style="color: red">*</span>
          </lable>
          <input type="text" name="fullName" id="fullName" class = "fullName">
        </div>

        <div class="user_input">
          <lable>Giới tính
            <span style="color: red">*</span>
          </lable>
            <?php
            $gender = array('0' => 'Nam', '1' => 'Nữ');
            for ($i = 0; $i < count($gender); $i++) {
              echo '
                <input type="radio" id="gender" name="gender" class = "gender" value="' . $gender[$i] . '">
                ';
              echo '
                <label>' . $gender[$i] . '</label> 
                ';
            }
            ?>
        </div>

        <div class="user_input">
          <lable>Phân khoa
            <span style="color: red">*</span>
          </lable>
            <select id="khoa" name="khoa" id="khoa" style="height: inherit">
              <?php
              $khoa = array(
                "0" => "",
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu",
              );
              
              foreach ($khoa as &$value) {
                echo '<option id = "khoa" value="' . $value . '">' . $value .'</option>';
              }
              ?>
            </select>
        </div>

        <div class="user_input" date-date-format="dd/MM/yyyy">
          <lable>Ngày sinh
            <span style="color: red">*</span>
          </lable>
          <input type="text" name="birthday" id="birthday" class = "birthday" placeholder="dd/mm/yyyy">
        </div>

        <div class="user_input">
          <lable>Địa chỉ</lable>
          <input type="text" name="address" id="address" class = "address">
        </div>

        <div class="user_input">
          <lable>Hình ảnh</lable>
          <input type="file" id="myFile" name="filename" class = "image">
        </div>

        <div class ="user_submit">
          <input type="submit" name="btnSubmit" id="form_btn" value="Đăng ký">
        </div>

      </form>
  </div>
  <?php
  ?>
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <!-- Bootstrap -->
  <!-- Bootstrap DatePicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- Bootstrap DatePicker -->
  <script type="text/javascript">
    $(function() {
      $('#birthday').datepicker({
        format: "dd/mm/yyyy"
      });
    });
  </script>
  
</body>

</html>