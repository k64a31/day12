<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="CSS/listTheme.css" type="text/css">
</head>
<title>Danh Sách Sinh Viên</title>
<body>
  <div class = "center">
    <div class="container">
        <div class="search">
            <form>
				<table class="search-table" >
          
					<tr>
						<td> <label>Khoa</label> </td>
						<td> 
							
            <select class="khoa" name="khoa" id="khoa">
              <?php
              $khoa = array(
                "0" => "",
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu",
              );
              
              foreach ($khoa as &$value) {
                echo '<option class = "khoa" value="' . $value . '">' . $value .'</option>';
              }
              ?>
            </select>
							
						</td>
					</tr>
					
					<tr>
						<td> <label>Từ khóa</label> </td>
						<td> 
            				<input type="text" name="keyword" id="keyword" class = "keyword"> 
						</td>
					</tr>

					
					<tr>
						<td><button id="delete" class="button-submit" onclick = "deleteFunc();" type="button">Xóa</button></td>
						<td> <button id="search" class="button-submit" onclick = "searchFunc();" type="submit">Tìm kiếm</button> </td>
					</tr>
					
				</table>
			</form>
        </div>

		<table class="count-table">
			<tr>
				<td>
        <span>Số sinh viên tìm thấy: XXX </span></td>
				<td>
					<a href="./index.php">
						<button class="add-btn">Thêm</button>
					</a>
				</td>
			</tr>
		</table>
		
        <table class="list-table" >
            <thead class="table-head">
                <tr>
                    <th class = "col1"> No </th>
                    <th class = "col2"> Tên sinh viên </th>
                    <th class = "col3"> Khoa </th>
                    <th class = "col4"> Action </th>
                </tr>
            </thead>
            <tbody>
                <tr>
					<td class = "col1">1</td>
					<td class = "col2">Nguyễn Văn A</td>
					<td class = "col3">Khoa học máy tính</td>
					<td>
						<button class="alter">Xóa</button>
						<button class="alter">Sửa</button>
					</td>
				</tr>
				
				<tr></tr>
				
				<tr>
					<td class = "col1">2</td>
					<td class = "col2">Trần Thị B</td>
					<td class = "col3">Khoa học máy tính</td>
					<td>
						<button class="alter">Xóa</button>
						<button class="alter">Sửa</button>
					</td>
				</tr>
				
				<tr></tr>
				
				<tr>
					<td class = "col1">3</td>
					<td class = "col2">Nguyễn Hoàng C</td>
					<td class = "col3">Khoa học vật liệu</td>
					<td>
						<button class="alter">Xóa</button>
						<button class="alter">Sửa</button>
					</td>
				</tr>
				
				<tr></tr>
				
				<tr>
					<td class = "col1">4</td>
					<td class = "col2">Đinh Quang D</td>
					<td class = "col3">Khoa học vật liệu</td>
					<td>
						<button class="alter">Xóa</button>
						<button class="alter">Sửa</button>
					</td>
				</tr>
            </tbody>
        </table>
    </div>
  <div>
  
</body>
<script>
	function deleteFunc(){
		document.getElementById("khoa").value = "";
		document.getElementById("keyword").value = "";
		sessionStorage.clear();	
	}
	function searchFunc(){
		
		sessionStorage.setItem("khoa", document.getElementById("khoa").value);
        sessionStorage.setItem("keyword", document.getElementById("keyword").value);
	}
	if (sessionStorage.getItem("khoa") != null) {
        	khoa.value = sessionStorage.getItem("khoa");
    	}
	if (sessionStorage.getItem("keyword") != null) {
		keyword.value = sessionStorage.getItem("keyword");
	}	
</script>


</html>